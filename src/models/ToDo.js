const mongoose = require("mongoose");

const ToDoSchema = new mongoose.Schema(
	{
		door: { type: Number, default: 0 },
		pallet: { type: String, default: "" },
		qtd: { type: Number, default: 0 },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("ToDo", ToDoSchema);
