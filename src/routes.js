const express = require("express");
const routes = express.Router();

/**********************/
/*** ToDoController ***/
/**********************/
const ToDoController = require("./controllers/ToDoController");
routes.get("/todo", ToDoController.getAll);
routes.get("/todo/:id", ToDoController.getByID);
routes.post("/todo", ToDoController.incluir);
routes.put("/todo/:id", ToDoController.editar);
routes.delete("/todo/:id", ToDoController.excluir);

routes.get("/", (req, res) => {
	return res.json({ msg: "Servico On-line" });
});

module.exports = routes;
