const mongoose = require("mongoose");
const ToDo = mongoose.model("ToDo");

module.exports = {
	async getAll(req, res) {
		const registros = await ToDo.find({});
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await ToDo.findById(req.params.id);
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await ToDo.create(req.body);
		req.io.emit("newToDo", registro);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await ToDo.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
		});
		req.io.emit("editToDo", registro);
		return res.send(registro);
	},

	async excluir(req, res) {
		await ToDo.findByIdAndRemove(req.params.id);
		req.io.emit("delToDo", req.params.id);
		return res.send();
	},
};
