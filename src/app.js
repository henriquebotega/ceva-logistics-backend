const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

mongoose.connect(
	"mongodb+srv://usuario:senha@banco.sxxny.mongodb.net/test?retryWrites=true&w=majority",
	{
		useCreateIndex: true,
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	}
);

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = require("http").Server(app);
const io = require("socket.io")(server);

app.use((req, res, next) => {
	req.io = io;
	return next();
});

require("./models/ToDo");
app.use("/api", require("./routes"));

server.listen(process.env.PORT || 3333);
